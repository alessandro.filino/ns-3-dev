#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/network-module.h"
#include "ns3/flow-monitor-module.h"


/*  NETWORK APPLICATION: FIRST ASSIGNMENT - THROUGHPUT CALCULATOR
*
*   PROTOCOL: TPC
*
*   ADDRESS: 10.1.1.0/24
*
*   node    node    node     node
*    n0      n1      n2       n3
*    |        |      |        |
*    =========================
*               LAN
*   
*   n0 -> n1 | n2 -> n3
*
*/
using namespace ns3;

NS_LOG_COMPONENT_DEFINE("Assignment1-Filino");

void createTrafficNode(const std::string& sender, const std::string& receiver, Ipv4InterfaceContainer* interfaces, NodeContainer* nodes, uint16_t port, uint32_t packetSize, uint32_t maxBytes, double startTime, double stopTime)
{
    uint32_t senderAddress = Names::Find<Node>(sender)->GetId();
    uint32_t receiverAddress = Names::Find<Node>(receiver)->GetId();

    BulkSendHelper bulk("ns3::TcpSocketFactory", InetSocketAddress(interfaces->GetAddress(receiverAddress), port));
    bulk.SetAttribute("SendSize", UintegerValue(packetSize));
    bulk.SetAttribute("MaxBytes", UintegerValue(maxBytes));
    
    ApplicationContainer bulkApps = bulk.Install(nodes->Get(senderAddress));

    PacketSinkHelper sink("ns3::TcpSocketFactory", InetSocketAddress(interfaces->GetAddress(receiverAddress), port));
    ApplicationContainer sinkApps = sink.Install(nodes->Get(receiverAddress));

    bulkApps.Start(Seconds(startTime));
    bulkApps.Stop(Seconds(stopTime));

    sinkApps.Start(Seconds(startTime));
    sinkApps.Stop(Seconds(stopTime));
}


int main(int argc, char* argv[])
{
    uint32_t nNodes = 4;
    std::string dataRate;
    double totalThroughput = 0.0;
    
    LogComponentEnable("Assignment1-Filino", LOG_LEVEL_INFO);
    
    CommandLine cmd(__FILE__);  
    cmd.AddValue("dataRate", "Set the data rate for CsmaChannel (e.g., --DataRate=10Mbps)", dataRate); 
    cmd.Parse(argc, argv);
    
    NodeContainer nodes;
    nodes.Create(nNodes);

    for(uint32_t i = 0; i < nNodes; i++)
    {
        std::string nodeName = "n" + std::to_string(i);
        Names::Add(nodeName, nodes.Get(i));
    }
    
    CsmaHelper csmaNodes;
    csmaNodes.SetChannelAttribute("DataRate", StringValue(dataRate.empty() ? "100Mbps" : dataRate));
    csmaNodes.SetChannelAttribute("Delay", TimeValue(NanoSeconds(6560)));

    NetDeviceContainer csmaDevices;
    csmaDevices = csmaNodes.Install(nodes);

    InternetStackHelper internetStack;
    internetStack.Install(nodes);

    Ipv4AddressHelper address;
    address.SetBase("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer csmaInterfaces;
    csmaInterfaces = address.Assign(csmaDevices);

    createTrafficNode("n0", "n1", &csmaInterfaces, &nodes , 9, 1024, 0, 0.0, 10.0);
    createTrafficNode("n2", "n3", &csmaInterfaces, &nodes , 9, 1024, 0, 0.0, 10.0);
   
    FlowMonitorHelper flowMonitor;
    Ptr<FlowMonitor> monitor = flowMonitor.Install(nodes);

    csmaNodes.EnablePcapAll("csma", true);

    Simulator::Stop(Seconds(10));
    Simulator::Run();
    
    monitor->SerializeToXmlFile("./scratch/stats.xml", true, true);

    Simulator::Destroy();

    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier>(flowMonitor.GetClassifier());
    std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats();

    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin(); i != stats.end(); ++i)
    {
        Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow(i->first);

        NS_LOG_INFO("FLOW: " << i->first);
        NS_LOG_INFO("SOURCE: " << t.sourceAddress);
        NS_LOG_INFO("DESTINATION: " << t.destinationAddress);
        NS_LOG_INFO("PACKET AVERAGE LOSS: " << (i->second.lostPackets * 100.0) / i->second.rxBytes);
        double throughput = i->second.rxBytes * 8.0 / (10.0 * 1000000);
        NS_LOG_INFO("THROUGHPUT: " << throughput <<" Mbps \n");
        totalThroughput += throughput;
    }
    
    NS_LOG_INFO("TOTAL THROUGHPUT: " << totalThroughput << "Mbps");
    
    return 0;
}
