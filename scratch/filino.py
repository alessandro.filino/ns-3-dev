import xmltodict
import os
import matplotlib.pyplot as plt

data_rates = ["10Mbps", "20Mbps", "30Mbps", "40Mbps", "50Mbps", "60Mbps", "70Mbps", "80Mbps", "90Mbps", "100Mbps"]

totalThroughputList = []
totalPacketLostList = []

for data_rate in data_rates:
    totalThroughput = 0.0
    totalPacketLost = 0.0
    throughput = 0.0

    command = './ns3 run scratch/filino.cc -- --dataRate=' + data_rate
    os.system(command)
    
    stats_file_path = './scratch/stats.xml'

    with open(stats_file_path) as xml_file:
        monitor_dict = xmltodict.parse(xml_file.read())
    
    flow_stats = monitor_dict["FlowMonitor"]["FlowStats"]

    for flow in flow_stats["Flow"]:
        throughput = float(flow["@rxBytes"]) * 8.0 / (10.0 * 1000000)
        totalThroughput += throughput
        packetLost = float(flow["@lostPackets"])
        totalPacketLost += packetLost
    
    totalThroughputList.append(round(totalThroughput,3))
    totalPacketLostList.append(totalPacketLost)

for i, data_rate in enumerate(data_rates):
    print("Total Throughput for", data_rate, ":", totalThroughputList[i], "with: ", totalPacketLostList[i], " lost packets")
    plt.bar(data_rate, totalThroughputList[i])

plt.xlabel("Total Throughut")
plt.ylabel("Data Rate")
plt.show()



