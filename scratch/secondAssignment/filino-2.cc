#include "ns3/log.h"
#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/spectrum-wifi-helper.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/ssid.h"
#include "ns3/mobility-helper.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/on-off-helper.h"
#include "ns3/core-module.h"


/*  NETWORK APPLICATION: SECOND ASSIGNMENT
*
*   PROTOCOL: TCP
*   NETWORK AP: 10.0.x.1 (dove x = i-esimo AP)
*   NETWORK STA: 10.0.x.y (dove y = j-esima STA)

*   
*   N. AP = nAccessPoints
*   N. STA = nSta (nSta per ogni AP) => nSta totali = totalSta
*                           
*                     STA_[0 nSta]                       STA_[1 nSta]                
*                       *                                  *
*                        \                                /               
*                         \                              /
*        apToStaDistance   \        apToApDistance      /         apToApDistance
*    *   ---------------- [AP0]   -----------------   [AP1]     -----------------     ....
                          ssid[0]                     ssid[1] \  
*   STA_[0 i]              /                            |      \
*                         /                             |       * STA_[1 i]  
*                        /                              |      /         
*                       *                               |     /  staTostaDistance   
*                   STA_[0 1]                           |    /
*                                                       *
*                                                      STA_[1 0]
*
*/

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("Assignment2-Filino");
int nInterferenceDropPhyRx = 0;

Ptr<YansWifiChannel> createYansChannel()
{
    YansWifiChannelHelper yansChannel;
    yansChannel.AddPropagationLoss("ns3::FriisPropagationLossModel",
                                "Frequency",
                                DoubleValue(2.412e9));
    
    yansChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");   
    return yansChannel.Create();
}

Ptr<MultiModelSpectrumChannel> createSpectrumChannel()
{
    Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
    lossModel->SetFrequency(2.412e9);
    Ptr<ConstantSpeedPropagationDelayModel> delayModel =
        CreateObject<ConstantSpeedPropagationDelayModel>();

    Ptr<MultiModelSpectrumChannel> spectrumChannel =
        CreateObject<MultiModelSpectrumChannel>();
    spectrumChannel->AddPropagationLossModel(lossModel);
    spectrumChannel->SetPropagationDelayModel(delayModel);

    return spectrumChannel;
}

void configureWifiDevices(const std::string &type, const Ssid &ssid_name, WifiHelper &wifiHelper, WifiMacHelper &macHelper, WifiPhyHelper &phyHelper, const NodeContainer &node, NetDeviceContainer &netDevice)
{
    macHelper.SetType(type, "Ssid", SsidValue(ssid_name));
    NetDeviceContainer device;
    netDevice.Add(wifiHelper.Install(phyHelper, macHelper, node));
}

ApplicationContainer createSinkApplication(std::string protocolType, uint16_t port, Ptr<Node> node, double simulationTime) 
{
    PacketSinkHelper packetSinkHelper(protocolType, InetSocketAddress(Ipv4Address::GetAny(), port));
    ApplicationContainer sinkApp;
    sinkApp = packetSinkHelper.Install(node);
    sinkApp.Start(Seconds(0.0));
    sinkApp.Stop(Seconds(simulationTime + 1));
    return sinkApp;
}

ApplicationContainer createOnOffApplication(std::string protocolType, uint16_t port, Ptr<Node> srcNode, Ipv4Address dstNode, double simulationTime, double payload) 
{
    OnOffHelper onoff(protocolType, Ipv4Address::GetAny());
    onoff.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
    onoff.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
    onoff.SetAttribute("PacketSize", UintegerValue(payload));
    onoff.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
    AddressValue remoteAddress(InetSocketAddress(dstNode, port));
    onoff.SetAttribute("Remote", remoteAddress);
    ApplicationContainer onOffApp = onoff.Install(srcNode);
    onOffApp.Start(Seconds(1.0));
    onOffApp.Stop(Seconds(simulationTime + 1));

    return onOffApp;
}

double calculateThroughput(const ApplicationContainer &sinkApp, Time simulationTime)
{
    uint64_t totalBytesRx = DynamicCast<PacketSink>(sinkApp.Get(0))->GetTotalRx();
    double throughput = totalBytesRx * 8 / (simulationTime.GetSeconds() * 1000000.0); // Mbit/s

    return throughput;
}

void wifiPhyRxFailure(ns3::Ptr<ns3::Packet const> packet, WifiPhyRxfailureReason reason)
{
    if(reason == PREAMBLE_DETECT_FAILURE)
    {
        nInterferenceDropPhyRx++;
    }
}

int main(int argc, char* argv[])
{
    auto timeStart = std::chrono::high_resolution_clock::now();
    uint32_t nAccessPoints = 2;
    uint32_t nSta = 1;
    const uint32_t payload = 1024;
    Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(payload));
    std::string protocolType = "ns3::TcpSocketFactory";
    std::string wifiType = "ns3::YansWifiPhy";
    std::string errorModelType = "ns3::NistErrorRateModel";

    bool makeInterference = false;
    bool twoChannel = false;

    double simulationTime = 10;

    double apToStaDistance = 5;
    double apToApDistance = 100;

    std::ofstream file("scratch/secondAssignment/run.txt");

    CommandLine cmd(__FILE__);
    cmd.AddValue("wifiType", "select ns3::SpectrumWifiPhy or ns3::YansWifiPhy", wifiType);
    cmd.AddValue("nAccessPoints", "select the number of APs", nAccessPoints);
    cmd.AddValue("apToApDistance", "select the distance between the AP", apToApDistance);
    cmd.AddValue("simulationTime", "select the simulation time (in seconds)", simulationTime);
    cmd.AddValue("errorModelType", "select the error model", errorModelType);
    cmd.AddValue("twoChannel", "Select if you want to use 2 channel", twoChannel);
    cmd.AddValue("makeInterference", "Default is false. If is true, access point make interference", makeInterference);
    cmd.AddValue("apToStaDistance", "select the distance between ap and stas", apToStaDistance);
    cmd.AddValue("protocolType", "select ns3::TcpSocketFactory for TCP protocol or ns3::UdpSocketFactory for UDP", protocolType);
    cmd.AddValue("nSta", "select the number of STAs for AP", nSta);
    cmd.Parse(argc, argv);

    //Selezioniamo il numero di canali su cui provare a fare interferenza
    int channelIndex = 1;
    std::vector<std::string> channelNames;
    channelNames.push_back("1");
    if (twoChannel)
    {
        std::string secondChannelName = (makeInterference ? "2" : "6");
        channelNames.push_back(secondChannelName);
        channelIndex = 2;
    }

    file << "wifiType: "<< wifiType  << "\n" <<
            "AP:" << nAccessPoints << "\n" <<
            "STA:" << nSta << " (per ogni AP) \n";
    file << "ProtocolType: " << protocolType << "\n";
    
    uint32_t totalSta = nSta*nAccessPoints;

    NodeContainer wifiStaNodes;
    wifiStaNodes.Create(totalSta); //Ogni ap avrà nSta connesse
    NodeContainer wifiApNodes;
    wifiApNodes.Create(nAccessPoints);

    YansWifiPhyHelper yansPhy;
    SpectrumWifiPhyHelper spectrumPhy;

    StringValue DataRate("HtMcs4");
    
    WifiHelper wifi;
    wifi.SetStandard(WIFI_STANDARD_80211n);
    WifiMacHelper mac;

    //Creo una rete per ogni ap
    Ssid ssid[nAccessPoints];
    for (uint32_t i = 0; i < nAccessPoints; i++)
    {
        std::stringstream ssidName;
        ssidName << "AP-" << i;
        ssid[i] = Ssid(ssidName.str());
    }

    wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                                "DataMode",
                                DataRate,
                                "ControlMode",
                                DataRate);
    NetDeviceContainer ap;
    NetDeviceContainer sta;
    uint32_t indexSta = 0;

    if (wifiType == "ns3::YansWifiPhy")
    { 
        yansPhy.SetChannel(createYansChannel());
        yansPhy.Set("TxPowerStart", DoubleValue(1));
        yansPhy.Set("TxPowerEnd", DoubleValue(1));

        for (uint32_t i = 0; i < nAccessPoints; i++)
        {
            yansPhy.Set("ChannelSettings", StringValue("{" + channelNames[i % channelIndex] + ", 20, BAND_2_4GHZ, 0}"));
            configureWifiDevices("ns3::ApWifiMac", ssid[i], wifi, mac, yansPhy, wifiApNodes.Get(i), ap);
            file << "Channel AP" << i << ": " << channelNames[i % channelIndex] << ", 20, BAND_2_4GHZ, 0\n";
            std::cout << "CANALE: " << channelNames[i % channelIndex] << "\n";

            for (uint32_t j = 0; j < nSta; j++)
            {
                configureWifiDevices("ns3::StaWifiMac", ssid[i], wifi, mac, yansPhy, wifiStaNodes.Get(indexSta), sta);
                indexSta ++;
            }
        }   
        yansPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
        yansPhy.EnablePcapAll("scratch/secondAssignment/yansPcap/yans", true);

    }
    else if (wifiType == "ns3::SpectrumWifiPhy")
    {
        spectrumPhy.SetErrorRateModel(errorModelType);
        spectrumPhy.Set("TxPowerStart", DoubleValue(1));
        spectrumPhy.Set("TxPowerEnd", DoubleValue(1));
        spectrumPhy.SetChannel(createSpectrumChannel());
        for (uint32_t i = 0; i < nAccessPoints; i++)
        {
            spectrumPhy.Set("ChannelSettings", StringValue("{" + channelNames[i % channelIndex] + ", 20, BAND_2_4GHZ, 0}"));
            configureWifiDevices("ns3::ApWifiMac", ssid[i], wifi, mac, spectrumPhy, wifiApNodes.Get(i), ap);
            file << "Channel AP" << i << ": " << channelNames[i % channelIndex] << ", 20, BAND_2_4GHZ, 0\n";
            std::cout << "CANALE: " << channelNames[i % channelIndex] << "\n";
            
            for (uint32_t j = 0; j < nSta; j++)
            {
                configureWifiDevices("ns3::StaWifiMac", ssid[i], wifi, mac, spectrumPhy, wifiStaNodes.Get(indexSta), sta);
                indexSta++;
            }
        }
        spectrumPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
        spectrumPhy.EnablePcapAll("scratch/secondAssignment/spectrumPcap/spectrum", true);
    }
    else
    {
        NS_FATAL_ERROR("Unsupported WiFi type " << wifiType);
    }

    Config::Set("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/HtConfiguration/"
                        "ShortGuardIntervalSupported",
                        BooleanValue(false));

    MobilityHelper mobilityAp;
    MobilityHelper mobilitySta;
    Ptr<ListPositionAllocator> positionAllocAp = CreateObject<ListPositionAllocator>();
    Ptr<ListPositionAllocator> positionAllocSta = CreateObject<ListPositionAllocator>();
    double generalDistance = 0.0;

    file << "\n---  CONFIGURAZIONE SISTEMA  ---\n";

    for (uint32_t i = 0; i < nAccessPoints; i++)
    {
        file << "ap-"<< i  << " posizione: x: " << generalDistance<< "\n";
        Vector positionAp(generalDistance, 0, 0);
        positionAllocAp->Add(positionAp);
        for (uint32_t j = 0; j < nSta; j++)
        {
           double staTostaDistance = rand()%10;
           Vector positionSta((generalDistance + apToStaDistance), staTostaDistance, 0);
           positionAllocSta->Add(positionSta);
           file << "    sta-" << j << " posizione x: " << (generalDistance + apToStaDistance + rand()%5) << "  " << " posizione y: " << (staTostaDistance) << "\n";
        }
        generalDistance = generalDistance + apToApDistance;
    }
    mobilityAp.SetPositionAllocator(positionAllocAp);
    mobilityAp.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobilityAp.Install(wifiApNodes);
    mobilityAp.SetPositionAllocator(positionAllocSta);
    mobilityAp.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobilityAp.Install(wifiStaNodes);

    InternetStackHelper stack;
    stack.Install(wifiApNodes);
    stack.Install(wifiStaNodes);

    Ipv4AddressHelper address;
    address.SetBase("10.0.0.0", "255.255.255.0");
    Ipv4InterfaceContainer apInterface;
    Ipv4InterfaceContainer staInterface;

    indexSta = 0;
    for (uint32_t i = 0; i < nAccessPoints; i++)
        {
            Ipv4InterfaceContainer interface = address.Assign(ap.Get(i));
            apInterface.Add(interface);

            for (uint32_t j = 0; j < nSta; j++)
            {
                Ipv4InterfaceContainer interface = address.Assign(sta.Get(indexSta));
                staInterface.Add(interface);
                indexSta ++;
            }
            address.NewNetwork();
        }

    uint16_t port = 50000;
    ApplicationContainer sinkApps;
    ApplicationContainer onOffApps;
    file << "\n---  CONFIGURAZIONE ONOFF  ---\n";
    for (uint32_t i = 0 ; i < totalSta; i++)
    {
        sinkApps.Add(createSinkApplication(protocolType, port, wifiStaNodes.Get(i), simulationTime)); 
    }
    indexSta = 0;
    for (uint32_t i = 0 ; i < nAccessPoints; i++)
    {
        for (uint32_t j = 0; j < nSta; j++)
        {
            onOffApps.Add(createOnOffApplication(protocolType, port, wifiApNodes.Get(i), staInterface.GetAddress(indexSta), simulationTime, payload));
            file << apInterface.GetAddress(i) << "-->" << staInterface.GetAddress(indexSta) << "\n";
            indexSta++;
        }
    }

    Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyRxDrop", MakeCallback(wifiPhyRxFailure));
    FlowMonitorHelper flowMonitor;
    Ptr<FlowMonitor> monitor = flowMonitor.InstallAll();

    Simulator::Stop(Seconds(simulationTime + 1));
    Simulator::Run();
    Simulator::Destroy();

    flowMonitor.SerializeToXmlFile("scratch/secondAssignment/flowMonitor.xml", false, false);
    file << "\n---  ANALISI THROUGHPUT  ---\n";
    for (uint32_t i = 0; i < totalSta; i++){
        double throughput = calculateThroughput(sinkApps.Get(i), Seconds(simulationTime));
        file << "Troughput" << i << ": " << std::fixed << std::setprecision(2) << throughput << " Mbit/s" << "\n";
    }
    
    file << "\n---  ANALISI DURATA COMPUTAZIONE  ---\n";
    auto timeFinish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> duration = timeFinish - timeStart;
    double realDuration = duration.count();
    file << "Duration: " << realDuration << " s" << "\n";

    file << "\n---  ANALISI PACCHETTI DROPPATI  ---\n";
    file << "Packet Loss: " << nInterferenceDropPhyRx << "\n";

    file.close();
    
    return 0;
}